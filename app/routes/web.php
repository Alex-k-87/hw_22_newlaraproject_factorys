<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::singleton(\App\AdapterInterface::class, function () {
    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
    return new \App\MaxmindAdapter($reader);
//    return new \App\IpapiAdapter();
});

Route::get('/', function () {
    $url = 'https://github.com/login/oauth/authorize';
    $parameters = [
        'client_id' => env('OAUTH_CLIENT_ID'),
        'redirect_uri' => env('OAUTH_REDIRECT_URI'),
        'scope' => 'read:user, user:email'
    ];
    return view('welcome', ['url' => $url . '?' . http_build_query($parameters)]);
})->name('home');

Route::get('/callback', function (\Illuminate\Http\Request $request) {

$client = new \GuzzleHttp\Client();
    $response = $client->request('POST', 'https://github.com/login/oauth/access_token', [
        'form_params' => [
            'client_id' => env('OAUTH_CLIENT_ID'),
            'client_secret' => env('OAUTH_CLIENT_SECRET'),
            'code' => $request->get('code'),
            'redirect_uri' => env('OAUTH_REDIRECT_URI')
        ]
    ]);
//    dd($response->getBody()->getContents());// получить тело ответа
//exit;
//    $response = "access_token=847d85ab99a5a0c9cbdd0dc33222c101fcf66d55&scope=read%3Auser%2Cuser%3Aemail&token_type=bearer";
//    $result=[];
//    parse_str($response, $result);  //3строки хардкода, что-бы не травмировать гитхаб )
//    dd($result['access_token']);
//exit;

    $response = $response->getBody()->getContents();

    $result = [];
    parse_str($response, $result);

    $response = $client->request('GET', 'https://api.github.com/user', [
        'headers' => [
            'Authorization' => 'token ' . $result['access_token'],
        ]
    ]);

    $userInfo = json_decode($response->getBody()->getContents(), true);

    $response = $client->request('GET', 'https://api.github.com/user/emails', [
        'headers' => [
            'Authorization' => 'token ' . $result['access_token'],
        ]
    ]);

    $userEmails = json_decode($response->getBody()->getContents(), true);

    $email = null;
    foreach ($userEmails as $userEmail) {
        if ($userEmail['primary']===true){
            $email = $userEmail['email'];
            break;
        }
    }
    $user = \App\User::where('email', '=', $email)->get()->first();
    if(!$user) {
        $user = new \App\User();
        $user->name = $userInfo['name'];
        $user->email = $email;
        $user->email_verified_at = now();
        $user->password = \Illuminate\Support\Facades\Hash::make(\Illuminate\Support\Str::random(10));
        $user->remember_token = \Illuminate\Support\Str::random(10);
        $user->save();
    }
     \Illuminate\Support\Facades\Auth::login($user, true);
        //Аутентификация нашего пользователя

    return redirect()->route('home');
})->name('callback');



//Route::get('/r/{code}', function ($code, \App\AdapterInterface $adapter) {
Route::get('/r/{code}', function ($code) {
    $link = \App\Link::where('short_code', $code)->get()->first();
//    $link = \App\Link::where('short_code', $code)->value('source_link');
//    $adapter->parse(request()->ip());

    $city = 'Odessa';
    $countryCode = 'UA';

    $statistic = new \App\Statistic();
    $statistic->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
    $statistic->link_id = $link->id;
    $statistic->ip = request()->ip();
    $statistic->user_agent = request()->userAgent();
    $statistic->ip = request()->server('REMOTE_ADDR');
    $statistic->user_agent = request()->server('HTTP_USER_AGENT');
    $statistic->country_code = $countryCode;
    $statistic->city_name = $city;
    $statistic->save();

    return redirect($link->source_link);

});

Route::prefix('/admin')->middleware('auth')->group(function () {
    Route::prefix('/users')->group(function () {
        Route::get('/', '\\' . \App\Http\Controllers\UserController::class . '@index')->name('users.index');
        Route::get('/create', '\\' . \App\Http\Controllers\UserController::class . '@create')->name('users.create');
        Route::post('/', '\\' . \App\Http\Controllers\UserController::class . '@store')->name('users.store');
        Route::get('/{user}', '\\' . \App\Http\Controllers\UserController::class . '@show')->name('users.show');
        Route::get('/{user}/edit', '\\' . \App\Http\Controllers\UserController::class . '@edit')->name('users.edit');
        Route::match(['put', 'patch'], '/{user}/edit', '\\' . \App\Http\Controllers\UserController::class . '@update')->name('users.update');
        Route::delete('/{user}', '\\' . \App\Http\Controllers\UserController::class . '@destroy')->name('users.destroy');
    });
});
//Route::resources([
//   'users' => '\\' . \App\Http\Controllers\UserController::class   //Эквивалент^^^
//]);
Route::get('sign-up', '\App\Http\Controllers\SignUpController@index')->name('sign-up');
Route::post('sign-up', '\App\Http\Controllers\SignUpController@handle')->name('handle-sign-up');

Route::get('sign-in', '\App\Http\Controllers\SignInController@index')->name('login');
Route::post('sign-in', '\App\Http\Controllers\SignInController@handle')->name('handle-sign-in');


Route::get('logout', '\App\Http\Controllers\SignUpController@logout')
    ->name('logout')
    ->middleware('auth');

Route::middleware('auth')->group(function () {
    Route::get('/wall', '\App\Http\Controllers\WallController@index')->name('wall.index');
    Route::get('/create-post', '\App\Http\Controllers\WallController@create')->name('post.create');
    Route::post('/create-post', '\App\Http\Controllers\WallController@store')->name('post.store');
    Route::delete('/{post}', '\App\Http\Controllers\WallController@destroy')->name('posts.destroy');
});
