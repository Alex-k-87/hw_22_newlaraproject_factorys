<?php


namespace App;


use App\AdapterInterface;
use GeoIp2\Database\Reader;

class MaxmindAdapter implements AdapterInterface
{
    protected $reader;
    protected $record;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function getCityName()
    {
        return $this->record->city->name;
    }

    public function getCountryCode()
    {
        return $this->record->country->isoCode;
    }

    public function parse(string $ip)
    {
        //
        try {
            $this->record = $this->reader->city(request()->ip());
        } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
            $this->record = $this->reader->city(env('DEFAULT_IP_ADDR'));
        }
    }
}
