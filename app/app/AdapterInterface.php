<?php


namespace App;


interface AdapterInterface
{
    public function getCityName();

    public function getCountryCode();

    public function parse(string $ip);

}
