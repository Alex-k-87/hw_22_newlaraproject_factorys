<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
