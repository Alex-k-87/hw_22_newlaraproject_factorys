<?php


namespace App\Http\Controllers;


use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Link;
use Ramsey\Uuid\Uuid;


class WallController
{
    public function index()
    {
        $user = Auth::user();

        $posts = $user->posts()->latest()->get();

        return view('wall', ['post' => $posts]);
    }

    public function create()
    {
         return view('create-post');
    }

    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required|min:10|max:257'
        ]);
        $text = $request->get('text');

        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $match);

        foreach ($match[0] as $sourceLink){
            $link = new \App\Link();
            $link->short_code = uniqid();
            $link->source_link = uniqid();
            Auth::user()->links()->save($link);
            $text = str_replace($sourceLink, sprintf('<a href="%s">%s</a>',env('APP_URL') . '/r/' . $link->short_code, env('APP_URL') . '/r/' . $link->short_code), $text);
        }

        $post = new Post();
        $post->id = Uuid::uuid4()->toString();
        $post->text = $text;

//        Auth::user()->posts()->save();
        Auth::user()->posts()->save($post);
        return redirect()->route('wall.index');
    }
    public function destroy(\App\Post $post)
    {
        $post->delete();
        return redirect()->route('wall.index')
            ->with('message', 'Post ' . $post->id . ' was deleted');
    }
}
