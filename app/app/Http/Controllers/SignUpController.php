<?php


namespace App\Http\Controllers;



use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignUpController
{
 public function index()
 {
    return view('sign-up');
 }
 public function handle(Request $request)
 {
     $request->validate([
         'email' => 'required|email|unique:users,email',
         'password' => 'required|min:4|confirmed',   //   |confirmed
         'password_confirmation' => 'required|min:4'
     ]);

     $credentials = $request->only('email', 'password');

     $user = new User();
     $user->name = '';
     $user->email = $credentials['email'];
     $user->password = Hash::make($credentials['password']);
     $user->save();

     if (Auth::attempt($credentials)) {
           return redirect()->route('wall.index');
     }
 }

 public function logout()
 {
     Auth::logout();
     return redirect()->route('home');
 }
}
