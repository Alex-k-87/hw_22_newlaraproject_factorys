<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignInController
{
 public function index()
 {
    return view('sign-in');
 }
 public function handle(Request $request)
 {
     $request->validate([
         'email' => 'required|email|exists:users,email',
         'password' => 'required|min:4|'
     ]);

     $credentials = $request->only('email', 'password');

     if (Auth::attempt($credentials)) {
           return redirect()->route('wall.index');
     }
 }

}
