<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UserController
{
    public function index()
    {
        return view('admin.users.index', ['users' => \App\User::latest('id')->paginate(15)]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|confirmed',   //   |confirmed
            'password_confirmation' => 'required|min:4'
        ]);
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->email_verified_at = (new \DateTime())->format('Y-m-d H:i:s');
        $user->password = Hash::make($request->get('password'));
        $user->remember_token = Str::random(10);
        $user->save();

        return redirect()->route('users.index')
              ->with('message', 'User ' . $user->name . ' was created' );
    }

    public function show(\App\User $user)
    {

    }

    public function edit(\App\User $user)
    {
        return view('admin.users.update', ['user'=>$user]);

    }

    public function update(\App\User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id . ',id',
            'password' => 'required|min:4|confirmed',   //   |confirmed
            'password_confirmation' => 'required|min:4'
        ]);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();

        return redirect()->route('users.index')
            ->with('message', 'User ' . $user->name . ' was updated' );
    }

    public function destroy(\App\User $user)
    {
        $user->delete();
        return redirect()->route('users.index')
            ->with('message', 'User ' . $user->name . ' was deleted' );
    }

}
