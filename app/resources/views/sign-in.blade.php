<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Users CRUD</title>
</head>
<body style="background-color: dimgrey">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="post" action="{{route('handle-sign-in')}}">
                @csrf
                <fieldset>
                    <!-- Email input-->
                    <div class="form-group">
                        @if ($errors->has('email'))
                            <ul class="alert alert-danger">
                                @foreach($errors->get('email') as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <label class="col-md-4 control-label" for="email">Email</label>
                        <div class="col-md-4">
                            <input id="textinput" name="email" type="text"  class="form-control input-md" value="{{old('email')}}">
                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        @if ($errors->has('password'))
                            <ul class="alert alert-danger">
                                @foreach($errors->get('password') as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <label class="col-md-4 control-label" for="password">Password</label>
                        <div class="col-md-4">
                            <input id="passwordinput" name="password" type="password"  class="form-control input-md">
                        </div>
                    </div>

                    <!-- Password confirmation-->
                    <div class="form-group">
                        @if ($errors->has('password_confirmation'))
                            <ul class="alert alert-danger">
                                @foreach($errors->get('password_confirmation') as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif


                    <!-- Button -->
                    <div class="form-group">
                        <div class="col-md-4">
                            <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Sign-In</button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
