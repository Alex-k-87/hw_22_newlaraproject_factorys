@extends('admin.layout')
@section('content')
 <form class="form-horizontal" method="post" action="{{route('users.update', $user->id)}}">
    @csrf

    @method('put')
    <fieldset>
        <!-- Form Name -->
        <legend>Update User</legend>

        <!-- Name input-->
        <div class="form-group">
            @if ($errors->has('name'))
                <ul class="alert alert-danger">
                    @foreach($errors->get('name') as $error)
                        <li>{{$error}}</li>
                        @endforeach
                </ul>
            @endif
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
                <input id="textinput" name="name" type="text" class="form-control input-md" value="{{old('name', $user->name)}}">
            </div>
        </div>

        <!-- Email input-->
        <div class="form-group">
            @if ($errors->has('email'))
                <ul class="alert alert-danger">
                    @foreach($errors->get('email') as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <input id="textinput" name="email" type="text"  class="form-control input-md" value="{{old('email', $user->email)}}">
            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            @if ($errors->has('password'))
                <ul class="alert alert-danger">
                    @foreach($errors->get('password') as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <input id="passwordinput" name="password" type="password"  class="form-control input-md">
            </div>
        </div>

        <!-- Password confirmation-->
        <div class="form-group">
            @if ($errors->has('password_confirmation'))
                <ul class="alert alert-danger">
                    @foreach($errors->get('password_confirmation') as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <label class="col-md-4 control-label" for="password_confirmation">Password Confirmation</label>
            <div class="col-md-4">
                <input id="passwordinput" name="password_confirmation" type="password"  class="form-control input-md">
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <div class="col-md-4">
                <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Edit</button>
            </div>
        </div>

    </fieldset>
</form>
@endsection
