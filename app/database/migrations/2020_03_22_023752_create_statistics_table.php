<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('statistics', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->uuid('id');
            $table->unsignedBigInteger('link_id');
            $table->string('ip');
            $table->string('country_code');
            $table->string('city_name');
            $table->string('user_agent');
            $table->primary('id');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
